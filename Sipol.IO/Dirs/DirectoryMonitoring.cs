﻿using Sipol.IO.Dirs.Interfaces;
using System.IO;

namespace Sipol.IO.Dirs
{
    public class DirectoryMonitoring : FileSystemWatcher, IWorkingDirObservator
    {
        private bool PrevEnable = false;

        public bool Enable
        {
            get { return EnableRaisingEvents; }
            set { EnableRaisingEvents = value; }
        }

        public event FileSystemEventHandler OnAllChangesInDirectory;

        public DirectoryMonitoring(string path) : base(path)
        {
            DefaultSettingsAfterContruct();
        }

        public DirectoryMonitoring(string path, string filter) : base(path, filter)
        {
            DefaultSettingsAfterContruct();
        }

        public DirectoryMonitoring()
        {
            DefaultSettingsAfterContruct();
        }

        private void DefaultSettingsAfterContruct()
        {
            IncludeSubdirectories = false;
            Enable = false;

            Changed += DirectoryMonitoring_Changed;
            Deleted += DirectoryMonitoring_Changed;
            Created += DirectoryMonitoring_Changed;
            Renamed += DirectoryMonitoring_Renamed;
        }

        private void DirectoryMonitoring_Renamed(object sender, RenamedEventArgs e)
        {
            OnAllChangesInDirectory?.Invoke(sender, e);
        }

        private void DirectoryMonitoring_Changed(object sender, FileSystemEventArgs e)
        {
            OnAllChangesInDirectory?.Invoke(sender, e);
        }

        public void UpdateWorkingDirectory(IWorkingDir workingDir)
        {
            SaveEnableRaisingEvents();
            Stop();
            Path = workingDir.Path;
            RestoreEnableRaisingEvents();
        }

        public void Start()
        {
            Enable = true;
        }

        public void Stop()
        {
            Enable = false;
        }

        private void SaveEnableRaisingEvents()
        {
            PrevEnable = Enable;
        }

        private void RestoreEnableRaisingEvents()
        {
            Enable = PrevEnable;
        }


    }
}
