﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Sipol.IO.Dirs
{
    public class DirectoryFilesLister
    {
        public string Path { get; private set; } = String.Empty;
        public string Filter { get; private set; } = String.Empty;
        public List<FileInfo> Files { get; private set; } = null;
        public int Count => Files?.Count ?? 0;

        public DirectoryFilesLister(string path, string filter = null)
        {
            if (String.IsNullOrWhiteSpace(path)) throw new NullReferenceException("path is empty (" + GetType().FullName + ")");
            Path = path;
            Filter = filter;
            Files = new List<FileInfo>();
            ReadFiles();            
        }

        public void ReadFiles()
        {
            Files.Clear();
            DirectoryInfo dir = DirectoryInfoExtensions.CreateDirectoryInfo(Path);

            if (String.IsNullOrWhiteSpace(Filter)) Files.AddRange(dir.GetFiles());
            else Files.AddRange(dir.GetFiles(Filter));
        }


    }
}
