﻿using Sipol.IO.Dirs.Interfaces;
using Sipol.IO.Paths;
using System;
using System.Collections.Generic;
using System.IO;

namespace Sipol.IO.Dirs
{
    public class WorkingSubDir : IWorkingDir, IWorkingDirObservator, IWorkinDirObservable
    {

        private string _path;
        private string _previousPath;
        private string _subDirectoryName = null;

        public WorkingSubDir(IWorkingDir workingDir, string subDirectoryName)
        {
            WorkingDir = workingDir ?? throw new ArgumentNullException(nameof(workingDir));
            if (String.IsNullOrEmpty(subDirectoryName)) throw new ArgumentNullException(nameof(subDirectoryName));
            if (String.IsNullOrWhiteSpace(subDirectoryName)) throw new ArgumentNullException(nameof(subDirectoryName));

            Path = subDirectoryName;

            WorkingDir.AttachObservator(this);
        }

        public string SubDirectoryName
        {
            get => _subDirectoryName;
            set
            {
                Path = value;
            }
        }
        public IWorkingDir WorkingDir { get; private set; } = null;

        /// <summary>
        /// Attention: Set Path olny relative as Working.Path directory
        /// </summary>
        public string Path
        {
            get => _path;
            set
            {
                SetPathAndSubDirName(value);
            }
        }


        public DirectoryInfo DirectoryInfo => new DirectoryInfo(Path);            
        public string PreviousPath { get => _previousPath; }
        public DirectoryInfo PreviousDirectoryInfo => new DirectoryInfo(PreviousPath);

        private List<IWorkingDirObservator> Observators = null;

        #region IWorkinDirObservable implementation

        public void NotifyObservators()
        {
            if (Observators == null) return;
            if (Observators.Count <= 0) return;

            foreach (IWorkingDirObservator observer in Observators)
            {
                if (observer != null)
                    observer.UpdateWorkingDirectory(this);
            }
        }


        private void CreateObservatorsList()
        {
            if (Observators == null) Observators = new List<IWorkingDirObservator>();
        }


        public bool IsInObservators(IWorkingDirObservator observator)
        {
            if (Observators == null) return false;
            return Observators.IndexOf(observator) >= 0;
        }


        public void AttachObservator(IWorkingDirObservator observator)
        {
            if (observator == null) return;
            CreateObservatorsList();
            if (IsInObservators(observator)) return;
            Observators.Add(observator);
        }


        public void DetachObservator(IWorkingDirObservator observator)
        {
            if (observator == null) return;
            if (!IsInObservators(observator)) return;
            Observators.Remove(observator);
        }


        public void DetachAllObservators()
        {
            if (Observators == null) return;
            Observators.Clear();
        }


        #endregion

        #region  IWorkingDirObservator implementation

        public void UpdateWorkingDirectory(IWorkingDir workingDir)
        {
            if (workingDir == null) return;
            if (workingDir != WorkingDir) return;
            if (String.Equals(WorkingDir.Path, WorkingDir.PreviousPath)) return;

            string subDirName = SubDirectoryName;
            Path = subDirName;
        }

        #endregion

        #region Help funcs

        public static string CreateSubDir(string parentDir, string subDirName)
        {
            if (String.IsNullOrWhiteSpace(subDirName))
                throw new ArgumentNullException(nameof(subDirName));

            if (!PathExt.IsValidPath(parentDir))
                throw new ArgumentException(nameof(parentDir) + " Is not valid");

            string path = parentDir + FS.Current.Path.DirectorySeparatorChar + subDirName;

            if (!PathExt.IsValidPath(path))
                throw new ArgumentException(nameof(subDirName) + " ["+(subDirName)+"] Is not valid, try use relative path");

            if (!FS.Current.Directory.Exists(path))
                FS.Current.Directory.CreateDirectory(path);

            return path;

        }


        private void SetPathAndSubDirName(string value)
        {
            if (!String.Equals(_path, _previousPath))
                _previousPath = _path;

            string val = value ?? String.Empty;
            val = val.Replace(WorkingDir.Path, "");
            if (val.Length > 0) val = val.TrimStart(FS.Current.Path.DirectorySeparatorChar);

            _path = CreateSubDir(WorkingDir.Path, val);
            _subDirectoryName = val;

            if (!String.Equals(_path, _previousPath))
                NotifyObservators();
        }

        #endregion
    }
}
