﻿using Sipol.IO.Dirs.Interfaces;
using System;
using System.IO;

namespace Sipol.IO.Dirs
{
    public class DefaultDirectory: IDirectoryInformation
    {
        public string Dir { get; private set; }
        public string Name { get; private set; }
        public DirectoryInfo DirectoryInfo => DirectoryInfoExtensions.CreateDirectoryInfo(Dir);

        public bool Exists => DirectoryInfo.Exists;

        public DefaultDirectory(string dir, string name = null)
        {
            Dir = dir;
            Name = name;
            createTmpDirIfNecessary();
        }

        private void createTmpDirIfNecessary()
        {
            if (String.IsNullOrWhiteSpace(Dir) || !IsExists()) createTemporaryDir();
        }

        private void createTemporaryDir()
        {
            TemporaryDirectory tmpDir = new TemporaryDirectory(Name);
            Dir = tmpDir.DirectoryPath;
            createDirectoryIfNotExists();
        }

        private void createDirectoryIfNotExists()
        {
            if (!IsExists()) DirectoryInfo.Create();
        }

        private bool IsExists()
        {
            return DirectoryInfo.Exists;
        }

    }
}
