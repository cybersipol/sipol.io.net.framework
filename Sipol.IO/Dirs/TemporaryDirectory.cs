﻿using System;

using System.IO;

namespace Sipol.IO.Dirs
{
    public class TemporaryDirectory
    {
        public string Name { get; private set; } = String.Empty;
        public string DirectoryPath => getDirectoryPath();

        public TemporaryDirectory(string name)
        {
            Name = name ?? String.Empty;
        }

        private string getDirectoryPath()
        {
            string tmpPath = Path.GetTempPath();

            if (String.IsNullOrWhiteSpace(Name)) return tmpPath;

            return tmpPath.TrimEnd(Path.DirectorySeparatorChar) + Path.DirectorySeparatorChar + Name;
        }

    }
}
