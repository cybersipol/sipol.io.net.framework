﻿using System;
using System.IO;

namespace Sipol.IO.Dirs
{
    public static class DirectoryInfoExtensions
    {
        public static DirectoryInfo CreateDirectoryInfo(string dir)
        {
            if (String.IsNullOrWhiteSpace(dir)) throw new NullReferenceException("Dir path is NULL or EMPTY (funtion Sipol.IO.Dirs.DirectoryInfoExtensions.CreateDirectoryInfo)");
            DirectoryInfo di = new DirectoryInfo(dir);
            di.Refresh();
            return di;
        }

}
}
