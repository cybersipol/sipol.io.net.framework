﻿namespace Sipol.IO.Dirs.Interfaces
{
    public interface IWorkinDirObservable
    {
        void AttachObservator(IWorkingDirObservator observator);
        void DetachObservator(IWorkingDirObservator observator);
        void DetachAllObservators();
        bool IsInObservators(IWorkingDirObservator observator);
        void NotifyObservators();

    }
}
