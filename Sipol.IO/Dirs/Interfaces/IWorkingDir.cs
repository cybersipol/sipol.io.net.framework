﻿using System.IO;

namespace Sipol.IO.Dirs.Interfaces
{
    public interface IWorkingDir
    {
        string Path { get; set; }
        DirectoryInfo DirectoryInfo { get; }

        string PreviousPath { get; }
        DirectoryInfo PreviousDirectoryInfo { get; }

        void AttachObservator(IWorkingDirObservator observator);
        void DetachAllObservators();
        void DetachObservator(IWorkingDirObservator observator);
        bool IsInObservators(IWorkingDirObservator observator);
    }
}