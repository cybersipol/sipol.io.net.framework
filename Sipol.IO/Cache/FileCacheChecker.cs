﻿using Sipol.IO.Cache.Interfaces;
using System;

namespace Sipol.IO.Cache
{
    public class FileCacheChecker : ICacheChecker
    {
        public virtual bool IsInCache(string name)
        {
            if (String.IsNullOrWhiteSpace(name)) return false;
            return FS.File.Exists(name);
        }

    }
}
