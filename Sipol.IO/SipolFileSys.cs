﻿using System.IO.Abstractions;

namespace Sipol.IO
{
    public class SipolFileSys : IFileSystem
    {
        private bool SettersOn = false;

        private FileBase FileBaseInstance = null;
        private DirectoryBase DirectoryBaseInstance = null;
        private PathBase PathBaseInstance = null;
        private IFileInfoFactory FileInfoFactoryInstance = null;
        private IFileStreamFactory FileStreamFactoryInstance = null;
        private IDirectoryInfoFactory DirectoryInfoFactoryInstance = null;
        private IDriveInfoFactory DriveInfoFactoryInstance = null;
        private IFileSystemWatcherFactory FileSystemWatcherFactoryInstance = null;

        private FileSystem DefaultFS = null; 


        public SipolFileSys(bool settersOn = false)
        {
            SettersOn = settersOn;
            DefaultFS = new FileSystem();
        }

        #region Implemented property

        public FileBase File
        {
            get
            {
                return GetFileBaseInstance();
            }

            set
            {
                if (SettersOn)
                    FileBaseInstance = value;
            }
        }

        public DirectoryBase Directory
        {
            get
            {
                return GetDirectoryBaseInstance();
            }

            set
            {
                if (SettersOn)
                    DirectoryBaseInstance = value;
            }
        }

        public IFileInfoFactory FileInfo
        {
            get
            {
                return GetFileInfoFactory();
            }

            set
            {
                if (SettersOn)
                    FileInfoFactoryInstance = value;
            }
        }

        public IFileStreamFactory FileStream
        {
            get
            {
                return GetFileStreamFactory();
            }

            set
            {
                if (SettersOn)
                    FileStreamFactoryInstance = value;
            }
        }

        public PathBase Path
        {
            get
            {
                return GetPathBaseInstance();
            }

            set
            {
                if (SettersOn)
                    PathBaseInstance = value;
            }
        }

        public IDirectoryInfoFactory DirectoryInfo
        {
            get
            {
                return GetDirectoryInfoFactory();
            }

            set
            {
                if (SettersOn)
                    DirectoryInfoFactoryInstance = value;
            }
        }

        public IDriveInfoFactory DriveInfo
        {
            get
            {
                return GetDriveInfoFactory();
            }

            set
            {
                if (SettersOn)
                    DriveInfoFactoryInstance = value;
            }
        }

        public IFileSystemWatcherFactory FileSystemWatcher
        {
            get
            {
                return GetFileSystemWatcherFactory();
            }

            set
            {
                if (SettersOn)
                    FileSystemWatcherFactoryInstance = value;
            }
        }

        #endregion

        #region Help funcs


        private FileBase GetFileBaseInstance()
        {
            if (FileBaseInstance == null)
                FileBaseInstance = DefaultFS.File;

            return FileBaseInstance;
        }


        private DirectoryBase GetDirectoryBaseInstance()
        {
            if (DirectoryBaseInstance == null)
                DirectoryBaseInstance = DefaultFS.Directory;

            return DirectoryBaseInstance;
        }


        private IFileInfoFactory GetFileInfoFactory()
        {
            if (FileInfoFactoryInstance == null)
                FileInfoFactoryInstance = DefaultFS.FileInfo;

            return FileInfoFactoryInstance;
        }


        private IFileStreamFactory GetFileStreamFactory()
        {
            if (FileStreamFactoryInstance == null)
                FileStreamFactoryInstance = DefaultFS.FileStream;

            return FileStreamFactoryInstance;
        }


        private PathBase GetPathBaseInstance()
        {
            if (PathBaseInstance == null)
                PathBaseInstance = DefaultFS.Path;

            return PathBaseInstance;
        }


        private IDirectoryInfoFactory GetDirectoryInfoFactory()
        {
            if (DirectoryInfoFactoryInstance == null)
                DirectoryInfoFactoryInstance = DefaultFS.DirectoryInfo;

            return DirectoryInfoFactoryInstance;
        }


        private IDriveInfoFactory GetDriveInfoFactory()
        {
            if (DriveInfoFactoryInstance == null)
                DriveInfoFactoryInstance = DefaultFS.DriveInfo;

            return DriveInfoFactoryInstance;
        }


        private IFileSystemWatcherFactory GetFileSystemWatcherFactory()
        {
            if (FileSystemWatcherFactoryInstance == null)
                FileSystemWatcherFactoryInstance = DefaultFS.FileSystemWatcher;

            return FileSystemWatcherFactoryInstance;
        }



        #endregion
    }
}
