﻿using Sipol.IO.Net.Interfaces;

using System.Net;

namespace Sipol.IO.Net
{
    public class WebClientWrapper : WebClient, IWebClient
    {
        public WebClientWrapper(): base()
        {
        }
    }
}
