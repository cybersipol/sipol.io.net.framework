﻿using Sipol.IO.Net.Interfaces;

using System;
using System.Net;

namespace Sipol.IO.Net
{
    public class WebDataLoader : IWebDataLoader, IDisposable
    {
        public IWebClient WebClient { get; private set; } = null;

        public WebDataLoader(IWebClient webClient)
        {
            WebClient = webClient ?? throw new ArgumentNullException(nameof(webClient));
        }

        public byte[] DownloadData(Uri url)
        {
            if (url == null)
                throw new ArgumentNullException(nameof(url));
            
            return WebClient.DownloadData(url);
        }

        public void Dispose()
        {
            WebClient.Dispose();
            WebClient = null;
        }
    }
}
