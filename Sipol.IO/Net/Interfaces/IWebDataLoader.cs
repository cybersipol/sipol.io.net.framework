﻿using System;

namespace Sipol.IO.Net.Interfaces
{
    public interface IWebDataLoader: IDisposable
    {
        byte[] DownloadData(Uri url);
    }
}
