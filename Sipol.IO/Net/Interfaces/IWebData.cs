﻿using System;

namespace Sipol.IO.Net.Interfaces
{
    public interface IWebData
    {
        Object WebData { get; set; }
    }
}
