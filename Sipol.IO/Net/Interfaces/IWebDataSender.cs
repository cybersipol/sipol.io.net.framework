﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sipol.IO.Net.Interfaces
{
    public interface IWebDataSender
    {
        string SendWebData();
    }
}
