﻿namespace Sipol.IO.Net.Interfaces
{
    public interface IWebClientFactory
    {
        IWebClient CreateWebClient();
    }
}
