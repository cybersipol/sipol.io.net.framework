﻿using Sipol.IO.Net.Interfaces;

using System;

namespace Sipol.IO.Net
{
    public class WebDataSender : IWebDataSender, IWebData
    {
        public WebDataSender( IWebClient webClient, Uri url, object data = null )
        {
            WebClient = webClient ?? throw new ArgumentNullException(nameof(webClient));
            Url = url ?? throw new ArgumentNullException(nameof(url));
            WebData = data;
        }

        public IWebClient WebClient { get; private set; } = null;
        public Uri Url { get; set; } = null;
        public object WebData { get; set; } = null;

        public string SendWebData()
        {
            return WebClient.UploadString(Url, "POST", WebData.ToString());
        }
    }
}
