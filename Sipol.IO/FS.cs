﻿using System.IO.Abstractions;

namespace Sipol.IO
{
    public static class FS
    {
        private static SipolFileSys FS_instance = null;

        public static SipolFileSys Current
        {
            get
            {
                if (FS_instance == null) FS_instance = new SipolFileSys(false);
                return FS_instance;
            }
            set
            {
                FS_instance = value;
            }
        }

        public static FileBase File { get => Current.File; set => Current.File = value; }

        public static DirectoryBase Directory { get => Current.Directory; set => Current.Directory = value; }

        public static IFileInfoFactory FileInfo { get => Current.FileInfo; set => Current.FileInfo = value; }

        public static IFileStreamFactory FileStream { get => Current.FileStream; set => Current.FileStream = value; }

        public static PathBase Path { get => Current.Path; set => Current.Path = value; }

        public static IDirectoryInfoFactory DirectoryInfo { get => Current.DirectoryInfo; set => Current.DirectoryInfo = value; }

        public static IDriveInfoFactory DriveInfo { get => Current.DriveInfo; set => Current.DriveInfo = value; }

        public static IFileSystemWatcherFactory FileSystemWatcher { get => Current.FileSystemWatcher; set => Current.FileSystemWatcher = value; }


        public static class SeparatorChar
        {
            public static char Directory => System.IO.Path.DirectorySeparatorChar;
            public static char AltDirectory => System.IO.Path.AltDirectorySeparatorChar;
            public static char Path => System.IO.Path.PathSeparator;
            public static char Volume => System.IO.Path.VolumeSeparatorChar;
            
        }

        public static class InvalidChars
        {
            private static char[] _FileNameChars = null;
            private static char[] _PathChars = null;

            public static char[] FileNameChars => GetFileNameChars();
            public static char[] PathChars => GetPathChars();

            private static char[] GetPathChars()
            {
                if (_PathChars == null) _PathChars = System.IO.Path.GetInvalidPathChars();
                return _PathChars;
            }

            private static char[] GetFileNameChars()
            {
                if (_FileNameChars == null) _FileNameChars = System.IO.Path.GetInvalidFileNameChars();
                return _FileNameChars;
            }
        }

        public static class Combine
        {
            public static string Paths(params string[] paths) => System.IO.Path.Combine(paths);
            public static string Paths(string path1, string path2) => System.IO.Path.Combine(path1, path2);
            public static string Paths(string path1, string path2, string path3) => System.IO.Path.Combine(path1, path2, path3);
            public static string Paths(string path1, string path2, string path3, string path4) => System.IO.Path.Combine(path1, path2, path3, path4);
        }

    }
}
