﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Http;
using Sipol.IO.Net.Interfaces;
using Sipol.IO.Net;

namespace Sipol.IO.Files
{
    public class UrlContentLoader: FileContentLoader, IDisposable
    {
        public Uri Url { get; private set; } 
        public IWebDataLoader WebDataLoader { get; set; }

        #region ctor 

        public UrlContentLoader(Uri url): base()
        {
            Url = url ?? throw new ArgumentNullException(nameof(url));
            Path = System.IO.Path.GetFileName(Url.LocalPath);
            WebDataLoader = null;
        }

        #endregion

        #region Overrides funcs

        public override byte[] GetFileContent()
        {
            if (WebDataLoader == null)
                WebDataLoader = new WebDataLoader(new WebClientWrapper());

            byte[] result = null;
            try
            {
                result = WebDataLoader.DownloadData(Url);
               }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }


        protected override bool IsFileExists()
        {
            return true;
        }

        public void Dispose()
        {
            if (WebDataLoader!=null)
                WebDataLoader.Dispose();
        }

        #endregion

    }
}
