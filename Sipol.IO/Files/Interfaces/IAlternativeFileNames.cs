﻿using System.Collections.Generic;

namespace Sipol.IO.Files.Interfaces
{
    public interface IAlternativeFileNames
    {
        string BaseName { get; }
        string Dir { get; }
        string Extension { get; }
        string FilePath { get; }
        List<string> SearchFileNames { get; }

        string SearchAlternativeFilesInDirectory();
        string SearchAlternativeFilesInDirectory(string dir);
    }
}