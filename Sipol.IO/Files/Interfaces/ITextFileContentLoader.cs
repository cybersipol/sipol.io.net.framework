﻿using System.Text;

namespace Sipol.IO.Files.Interfaces
{
    public interface ITextFileContentLoader
    {
        string GetFileTextContent(Encoding encoding);

    }
}
