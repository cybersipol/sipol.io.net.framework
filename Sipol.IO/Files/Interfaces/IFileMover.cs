﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sipol.IO.Files.Interfaces
{
    public interface IFileMover
    {
        string FilePath { get; }
        string DestinationDirectory { get; }

        void MoveFile();
    }
}
