﻿using Sipol.IO.Files.Interfaces;
using System;
using System.IO;

namespace Sipol.IO.Files
{
    public class FileExists : IFileExists
    {
        public bool IsExists(string path)
        {
            if (String.IsNullOrWhiteSpace(path)) return false;

            FileInfo f = new FileInfo(path);
            f.Refresh();
            return f.Exists;
        }
    }
}
