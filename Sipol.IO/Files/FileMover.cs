﻿using Sipol.IO.Files.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sipol.IO.Files
{
    public class FileMover : IFileMover
    {
        public FileMover(string filePath, string destinationDirectory)
        {
            FilePath = filePath ?? throw new ArgumentNullException(nameof(filePath) + " is null (" + GetType().FullName + ")");
            DestinationDirectory = destinationDirectory ?? throw new ArgumentNullException(nameof(destinationDirectory) + " is null (" + GetType().FullName + ")");
        }

        public string FilePath { get; private set; } = String.Empty;

        public string DestinationDirectory { get; private set; }

        public string DestinantionPath { get; private set; } = String.Empty;


        public void MoveFile()
        {
            string name = Path.GetFileName(FilePath);
            if (!FileExister.Exists(FilePath)) throw new FileNotFoundException("" + FilePath ?? "(null)");

            DestinantionPath = DestinationDirectory + Path.DirectorySeparatorChar + name;
            if (FileExister.Exists(DestinantionPath)) File.Delete(DestinantionPath);
            File.Move(FilePath, DestinantionPath);
        }
    }
}
