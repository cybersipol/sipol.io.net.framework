﻿using NUnit.Framework;
using Sipol.IO.Files;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Sipol.IO.Files.Tests.Unit
{
    [TestFixture()]
    public class FileContetntLoader_Tests
    {
        private FileContentLoaderSUT sut;

        [SetUp]
        public void Init()
        {
            FileExister.Reset();
            sut = new FileContentLoaderSUT("anyfile.txt");
        }

        [TearDown]
        public void End()
        {
            FileExister.Reset();
        }

        #region ctor Tests

        [Test()]
        public void ctor_NullPath_ThrowException()
        {
            Assert.That(() =>
            {
                object result = new FileContentLoaderSUT(null);
            },
            Throws.ArgumentNullException
            );
        }

        [Test()]
        public void ctor_EmptyPath_ThrowException()
        {
            Assert.That(() =>
            {
                object result = new FileContentLoaderSUT(String.Empty);
            },
            Throws.ArgumentNullException
            );
        }

        [Test()]
        public void ctor()
        {
            object result = new FileContentLoaderSUT("anyfile.txt");
            Assert.That(result, Is.Not.Null & Is.InstanceOf<FileContentLoader>());
        }
        #endregion


        #region GetFileContent Tests

        [Test()]
        public void GetFileContent_NotExistsFile_ThrowExpcetion()
        {
            FileExister.Set(false);

            Assert.That(() =>
            {
                sut.GetFileContent();
            },
            Throws.InstanceOf<FileNotFoundException>()
            );

        }


        [Test()]
        public void GetFileContent_FileExistsWithContent_ReturnBytesOfContent()
        {
            byte[] expectedContent = Encoding.UTF8.GetBytes("same text in file");
            Stream str = new MemoryStream(expectedContent);
            sut.ContentStream = str;
            FileExister.Set(true);

            byte[] result = sut.GetFileContent();

            
            Assert.That(result, Is.Not.Null & Is.EquivalentTo(expectedContent));

        }

        [Test()]
        public void GetFileContent_FileExistsAndIsEmpty_ReturnEmptyContent()
        {
            byte[] expectedContent = Encoding.UTF8.GetBytes("");
            Stream str = new MemoryStream(expectedContent);
            sut.ContentStream = str;
            FileExister.Set(true);

            byte[] result = sut.GetFileContent();

            Assert.That(result, Is.Not.Null & Is.EquivalentTo(expectedContent));

        }
        #endregion


        #region GetFileTextContent Tests

        [Test()]
        public void GetFileTextContent_EncodingNull_ThrowExcption()
        {
            Assert.That(() =>
            {
                sut.GetFileTextContent(null);
            },

            Throws.ArgumentNullException
            );
        }


        [Test()]
        public void GetFileTextContent_FileNotExists_ThrowExcption()
        {
            FileExister.Set(false);

            Assert.That(() =>
            {
                sut.GetFileTextContent(Encoding.UTF8);
            },
            Throws.InstanceOf<FileNotFoundException>()
            );

        }


        [Test()]
        public void GetFileTextContent_FileExists_ReturnText()
        {
            Encoding enc = Encoding.UTF8;
            string expectedText = "some text in some file";
            byte[] expectedContent = enc.GetBytes(expectedText);
            Stream str = new MemoryStream(expectedContent);
            sut.ContentStream = str;
            FileExister.Set(true);

            string result = sut.GetFileTextContent(enc);

            Assert.That(result, Is.Not.Null & Is.Not.Empty & Is.EqualTo(expectedText));
        }


        [Test()]
        public void GetFileTextContent_FileExistsAndIsEmpty_ReturnEmptyText()
        {
            Encoding enc = Encoding.UTF8;
            string expectedText = String.Empty;
            byte[] expectedContent = enc.GetBytes(expectedText);
            Stream str = new MemoryStream(expectedContent);
            sut.ContentStream = str;
            FileExister.Set(true);

            string result = sut.GetFileTextContent(enc);

            Assert.That(result, Is.Not.Null & Is.Empty & Is.EqualTo(expectedText));
        }

        #endregion

        #region BUFFOR_SIZE Tests

        [Test()]
        public void BUFFORSIZE_BufforZero_ThrowException()
        {
            byte[] expectedContent = Encoding.UTF8.GetBytes("same text in file");
            Stream str = new MemoryStream(expectedContent);
            sut.ContentStream = str;
            sut.BUFFOR_SIZE = 0;
            FileExister.Set(true);

            Assert.That(() => 
            { 
                byte[] result = sut.GetFileContent();
            },
            Throws.InstanceOf<ArgumentOutOfRangeException>()
            );

            Assert.That(() =>
            {
                string result = sut.GetFileTextContent(Encoding.ASCII);
            },
            Throws.InstanceOf<ArgumentOutOfRangeException>()
            );
        }


        #endregion

        #region Help classes

        class FileContentLoaderSUT : FileContentLoader
        {
            public FileContentLoaderSUT(string path): base(path)
            {
            }

            public Stream ContentStream { get => SwapStream; set { SwapStream = value; } }


            /*
            public byte[] GetFileContent(string path)
            {
                Path = path;
                return this.GetFileContent();
            }

            public string GetFileTextContent(string path, Encoding encoding)
            {
                Path = path;
                return this.GetFileTextContent(encoding);
            }
            */
        }

        #endregion

    }
}