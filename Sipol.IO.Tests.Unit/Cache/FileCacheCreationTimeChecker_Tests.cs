﻿using NSubstitute;
using NUnit.Framework;
using System;
using System.IO.Abstractions;

namespace Sipol.IO.Cache.Tests.Unit
{
    [TestFixture()]
    public class FileCacheCreationTimeChecker_Tests
    {
        private FileCacheCreationTimeChecker sut;
        private double daysInCache = 7d;


        [SetUp]
        public void Init()
        {
            FS.Current = new SipolFileSys(true);
            sut = new FileCacheCreationTimeChecker(TimeSpan.FromDays(daysInCache));
        }

        #region ctor Tests

        [Test()]
        public void ctor()
        {
            object result = new FileCacheCreationTimeChecker(TimeSpan.FromMinutes(5));
            Assert.That( result,
                            Is.Not.Null &
                            Is.InstanceOf<FileCacheCreationTimeChecker>()
                );
        }

        #endregion


        #region IsInCache Tests

        [TestCase(null)]
        [TestCase("")]
        [TestCase("   ")]
        public void IsInCache_EmptyNullOrWhitespaceName_ReturnFalse(string name)
        {
            bool result = sut.IsInCache(name);

            Assert.That(result,
                        Is.False
                        );

        }


        [Test()]
        public void IsInCache_NotExistsFileName_ReturnFalse()
        {
            FS.File = Substitute.For<FileBase>();
            string path = @"c:\some_test_file.txt";
            FS.File.Exists(path).Returns(false);
            bool result = sut.IsInCache(path);
            Assert.That(result,
                        Is.False
                        );
        }


        [Test()]
        public void IsInCache_CheckGetCreateTimeCall_IsCalledGetCreateTime()
        {
            FS.File = Substitute.For<FileBase>();
            string path = @"c:\some_test_file.txt";
            FS.File.Exists(path).Returns(true);

            bool result = sut.IsInCache(path);

            FS.File.Received().GetCreationTime(path);
        }


        [Test()]
        public void IsInCache_IfDifferenceNowAndFileTimeLessThanMaxTime_ReturnTrue()
        {
            FS.File = Substitute.For<FileBase>();
            string path = @"c:\some_test_file.txt";
            FS.File.Exists(path).Returns(true);
            FS.File.GetCreationTime(path).Returns(DateTime.Now.AddDays(-(daysInCache - 1d)));

            bool result = sut.IsInCache(path);

            Assert.That(result,
                        Is.True
                        );

        }


        [Test()]
        public void IsInCache_IfDifferenceNowAndFileTimeGreaterOrEqualMaxTime_ReturnFalse()
        {
            FS.File = Substitute.For<FileBase>();
            string path = @"c:\some_test_file.txt";
            FS.File.Exists(path).Returns(true);
            FS.File.GetCreationTime(path).Returns(DateTime.Now.AddDays(- (daysInCache + 1d)));

            bool result = sut.IsInCache(path);

            Assert.That(result,
                        Is.False
                        );

        }


        [Test()]
        public void IsInCache_MaxTimeLessThanZeroAndFileExists_ReturnTrueANDNotCalledGetCreationTime()
        {
            sut = new FileCacheCreationTimeChecker(TimeSpan.Zero - TimeSpan.FromDays(1));
            FS.File = Substitute.For<FileBase>();
            string path = @"c:\some_test_file.txt";
            FS.File.Exists(path).Returns(true);
            FS.File.GetCreationTime(path).Returns(DateTime.Now.AddDays(-(daysInCache + 1d)));

            bool result = sut.IsInCache(path);

            Assert.That(result,
                        Is.True
                        );

            FS.File.DidNotReceive().GetCreationTime(path);
        }


        [Test()]
        public void IsInCache_MaxTimeEqualZeroAndFileExists_ReturnTrueANDNotCalledGetCreationTime()
        {
            sut = new FileCacheCreationTimeChecker(TimeSpan.Zero);
            FS.File = Substitute.For<FileBase>();
            string path = @"c:\some_test_file.txt";
            FS.File.Exists(path).Returns(true);
            FS.File.GetCreationTime(path).Returns(DateTime.Now.AddDays(-(daysInCache + 1d)));

            bool result = sut.IsInCache(path);

            Assert.That(result,
                        Is.True
                        );

            FS.File.DidNotReceive().GetCreationTime(path);
        }


        [Test()]
        public void IsInCache_TimePointEndIsEqualMinValue_TimePointEndSetToNow()
        {
            FS.File = Substitute.For<FileBase>();
            string path = @"c:\some_test_file.txt";
            FS.File.Exists(path).Returns(true);

            sut.TimeEndPoint = DateTime.MinValue;
            bool result = sut.IsInCache(path);

            Assert.That(sut.TimeEndPoint,
                        Is.GreaterThanOrEqualTo(DateTime.Now.AddMinutes(-60))
                        );

        }

        [Test()]
        public void IsInCache_TimePointEndIsSet_TimePointEndNoChange()
        {
            FS.File = Substitute.For<FileBase>();
            string path = @"c:\some_test_file.txt";
            FS.File.Exists(path).Returns(true);

            DateTime expectedTime = DateTime.Today;

            sut.TimeEndPoint = expectedTime;
            bool result = sut.IsInCache(path);

            Assert.That(sut.TimeEndPoint,
                        Is.EqualTo(expectedTime)
                        );

        }
        #endregion
    }
}