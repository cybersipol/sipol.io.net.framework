﻿using NUnit.Framework;
using Sipol.IO.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace Sipol.IO.Net.Tests.Unit
{
    [TestFixture()]
    public class WebDataLoader_Tests
    {
        private WebDataLoader sut;

        [SetUp]
        public void Init()
        {
            sut = null;
        }

        #region ctor Tests

        [Test()]
        public void ctor_WebClientIsNull_ThrowException()
        {
            Assert.That(() =>
            {
                object result = new WebDataLoader(null);
            },
            Throws.ArgumentNullException
            );
        }

        [Test()]
        public void ctor()
        {
            WebClientWrapper cln = new WebClientWrapper();

            object result = new WebDataLoader(cln);

            Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<WebDataLoader>()
                );

        }

        #endregion


        #region DownloadData Tests

        [Test()]
        public void DownloadData_NullUrl_ThrowExpcetion()
        {
            WebClientWrapper cln = new WebClientWrapper();

            sut = new WebDataLoader(cln);

            Assert.That(() =>
            {
                byte[] result = sut.DownloadData(null);
            },
            Throws.ArgumentNullException
            );
        }

        #endregion

    }
}