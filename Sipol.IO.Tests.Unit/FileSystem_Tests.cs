﻿using NUnit.Framework;
using System;
using System.IO.Abstractions;

namespace Sipol.IO.Tests.Unit
{
    [TestFixture()]
    public class SipolFileSys_Tests
    {
        private SipolFileSys sut = null;

        [SetUp]
        public void Init()
        {
            sut = new SipolFileSys(false);
        }

        [TestCase("File",typeof(FileBase))]
        [TestCase("Directory", typeof(DirectoryBase))]
        [TestCase("FileInfo", typeof(IFileInfoFactory))]
        [TestCase("FileStream", typeof(IFileStreamFactory))]
        [TestCase("Path", typeof(PathBase))]
        [TestCase("DirectoryInfo", typeof(IDirectoryInfoFactory))]
        [TestCase("DriveInfo", typeof(IDriveInfoFactory))]
        [TestCase("FileSystemWatcher", typeof(IFileSystemWatcherFactory))]
        public void Getter_byDefault_ReturnInstance(string propertyName, Type typeExpected)
        {
            object result = sut.File;
            
            Assert.That(sut,
                        Is.Not.Null &
                        Has.Property(propertyName).Not.Null &
                        Has.Property(propertyName).InstanceOf(typeExpected)
            );
        }
    }
}
